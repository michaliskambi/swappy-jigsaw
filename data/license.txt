BlackBorder.png
WhiteBorder.png
CC0 by EugeneLoza

GameScreenLeft.png
GameScreenRight.png
CC0 by Talosaurus
https://opengameart.org/content/simple-smooth-menu-buttons

SwappyJigsawIcon.png
SwappyJigsawIcon.ico
CC0 by George Hodan

Star.png
CC0 by Photoshopwizard
https://opengameart.org/content/ui-pack-expansion

credits.png
made in GIMP
Font used: EptKazoo by Mark V. (markv@eptcomic.com), SIL Open Font License
Castle Game Engine Logo: Paweł Wojciechowicz, Michalis Kamburelis, GPLv2

Images in "img" folder:
PublicDomainPictures.net
by
George Hodan
Petr Kratochvil
Vera Kratochvil
Jean Beaufort
Kirk F
Rajesh Misra

Bobby Mikul
Svetlana Tikhonova
Paul Brennan
Jon Luty
Summer Woods

Mohamed Hassan
Rostislav Kralik
C. Colourin
Kai Stachowiak
Lebin Yuriy
Kathryn Smith
CC0 Community
Enzo Abramo
Patricia Keith
Linnaea Mallette
Iron Why
Lubos Houska
